# GeekPwnTTS
This is src code of textToSpeech.

https://gitee.com/ko-orz/GeekPwnTTS/

## 用法说明
1. util中的splitWav&silentWav.js可以做到切分原始语音，变成多个语音片段；
2. util中的pinYinUtil.js可以提取中文的拼音；
3. util中的getPinYinCount.js可以判断音节归档的完成度；
4. textToSpeech.js生成语音文件
## 技术说明
### 语音去噪
1. 判断语音数据中是否有连续的低于阈值的部分；
2. 有就直接将这部分声音置零；
### 语音分割
1. 将停顿点作为语音分割点；
2. 将语音切割为多个语音片段；
### 语音平滑处理
1. 找到语音片段的开头波段和结尾波段；
2. 判断波段是否在0附近，不在的话，剔除改周期，直到下周起的0值；
3. 这样处理以后，就不会出现波段不平滑，周期不自然的情况；
### 音节识别（未实现）
1. 将切好的语音与预设的语音作比较；
2. 相似度高则判定通过；
3. 需要通过HMM(Hidden Markov Model)/GMM(Gaussian Mixture Model)等的知识；
4. 需要训练音节模型；
### 归档音节
1. 自动归档识别好的音节；
2. 不仅是音节，还要做好整字，整词，整句的归档，提高可识别度；
3. 本处直接采用了人工归档，就是一个一个听，然后右键保存啦O(∩_∩)O;
### 录音器材猜想（未实现）
1. 通过直接录取不同频率的声音，建模会更好处理，因为现在的录音设备是把不同频率混在一起了，很难分离；
2. 该录音器可以参考人的耳蜗，是旋涡形状的，越到内部识别越高频率的声音，越外面识别月低评率的声音；
### 切词,产生顿挫感觉
1. 导入大量网络文本资源；
2. 建立一个字典，把文字循环丢入字典中；
3. 直接将出现次数最多的组合作为一个词；
4. 出现较少的可以通过TFIDF算法来提升这个词的权重；
5. 项目还未完善，本处直接使用了jieba切词；
## 参考资料
1. [声纹鉴定能否鉴定经过变声器的声音？](https://www.zhihu.com/question/38853154)
2. [什么是共振峰？](https://www.zhihu.com/question/24190826/answer/32419809)
3. [音频特征提取————常用音频特征](http://www.bubuko.com/infodetail-2055350.html)
4. [理解快速傅里叶变换（FFT）算法](http://blog.jobbole.com/58246/)
5. [用最简单的方式实现FFT](http://tieba.baidu.com/p/2513502552)
6. [Notes on the FFT](http://www.fftw.org/burrus-notes.html)
7. [FFT算法介绍](https://wenku.baidu.com/view/8bfb0bd476a20029bd642d85.html)
8. [视音频数据处理入门](http://blog.csdn.net/leixiaohua1020/article/details/50535042)
9. [声纹识别特征的特征参数提取](https://wenku.baidu.com/view/972457e06294dd88d1d26b06.html)
10. [中国汉子有多少个发音？](https://www.zhihu.com/question/20213109)
11. [国语拼音对照表](http://htmfiles.englishhome.org/pinyin/bopomofo.htm#English)
12. [wav格式解析](http://www.cnblogs.com/yangzizhen/p/4112763.html)
13. [世界上十个最伟大的公式](http://www.dugoogle.com/shijiezhizui/technology-22498/)



